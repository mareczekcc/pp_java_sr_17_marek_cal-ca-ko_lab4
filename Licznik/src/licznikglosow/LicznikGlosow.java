// ***********************************************************************
//  LicznikGlosow.java
//
//  Wykorzystuje GUI oraz listenery eventow do glosowania 
//  na dwoch kandydatow -- Jacka i Placka.
//
// ***********************************************************************
//package licznikglosow;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

public class LicznikGlosow extends JApplet
{
    private int APPLET_WIDTH = 300, APPLET_HEIGHT=100;
    private int glosyDlaJacek;
    private int glosyDlaPlacek;
    private JLabel labelJacek;
    private JLabel labelPlacek;
    private JButton Jacek;
    private JButton Placek;
    
    // ------------------------------------------------------------
    //  Ustawia GUI
    // ------------------------------------------------------------
    public void init ()
    {
	glosyDlaJacek = 0;
	glosyDlaPlacek =0;
	
	Jacek = new JButton ("Glosuj na Jacka!");
	Placek = new JButton ("Glosuj na Placka!");
	Jacek.addActionListener (new JacekButtonListener());
	Placek.addActionListener (new PlacekButtonListener());
	
	labelJacek = new JLabel ("Glosy dla Jacka: " + Integer.toString (glosyDlaJacek));
	labelPlacek = new JLabel ("Glosy dla Placka: " + Integer.toString (glosyDlaPlacek));
	
	Container cp = getContentPane();
	cp.setBackground (Color.cyan);
	cp.setLayout (new FlowLayout());
	cp.add (Jacek);
	cp.add (Placek);
	cp.add (labelJacek);
	cp.add (labelPlacek);

	setSize (APPLET_WIDTH, APPLET_HEIGHT);
    }


    // *******************************************************************
    //  Reprezentuje listener dla akcji wcisniecia przycisku
    // *******************************************************************
    private class JacekButtonListener implements ActionListener
    {
        public void actionPerformed (ActionEvent event)
        {
            glosyDlaJacek++;
            labelJacek.setText ("Glosy dla Jacka: " + Integer.toString (glosyDlaJacek)); 
            repaint ();
        }
    }
    private class PlacekButtonListener implements ActionListener
    {
        public void actionPerformed (ActionEvent event)
        {
            glosyDlaPlacek++;
            labelPlacek.setText ("Glosy dla Placka: " + Integer.toString (glosyDlaPlacek)); 
            repaint ();
        }
    }
}