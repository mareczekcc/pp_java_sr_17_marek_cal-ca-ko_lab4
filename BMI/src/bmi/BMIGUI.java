//********************************************************************
//  BMIGUI.java     
//
//  Wyznacza BMI w GUI.
//********************************************************************
package bmi;

import java.awt.*;
import java.awt.event.*;

import javax.swing.*;
import javax.swing.JOptionPane;


public class BMIGUI
{
   private int WIDTH = 300;
   private int HEIGHT = 220;

   private JFrame frame;
   private JPanel panel;
   private JLabel wzrostLabel, wagaLabel, BMILabel, wynikLabel, dodatkowa;
   public JTextField wzrost, waga;
   private JButton oblicz;

   //-----------------------------------------------------------------
   //  Ustawia GUI.
   //-----------------------------------------------------------------
   public BMIGUI()
   {
      frame = new JFrame ("Kalkulator BMI");
      frame.setDefaultCloseOperation (JFrame.EXIT_ON_CLOSE);

      //tworzenie etykiety dla pol tekstowych wzrostu i wagi 
      wzrostLabel = new JLabel ("Twoj wzrost w metrach:");
      wagaLabel = new JLabel ("Twoja waga w kilokramach: ");

      //stworz etykiete "to jsest twoje BMI" 
      BMILabel = new JLabel ("To jest Twoje BMI: ");
      //stworz etykiete wynik dla wartosci BMI
      wynikLabel = new JLabel ("");
      
      dodatkowa = new JLabel("");

      // stworz JTextField dla wzrostu
      wzrost = new JTextField(20);

      // stworz JTextField dla wagi
      waga = new JTextField(20);

      // stworz przycisk, ktory po wcisnieciu policzy BMI
      oblicz = new JButton ("Oblicz!");
      
      // stworz BMIListener, ktory bedzie nasluchiwal czy przycis zostal nacisniety 
      oblicz.addActionListener (new BMIListener());
      
      // ustawienia JPanel znajdujacego sie na JFrame 
      panel = new JPanel();
      panel.setPreferredSize (new Dimension(WIDTH, HEIGHT));
      panel.setBackground (Color.yellow);

      //dodaj do panelu etykiete i pole tekstowe dla wzrostu
      panel.add(wzrostLabel);
      panel.add(wzrost);
      //dodaj do panelu etykiete i pole tekstowe dla wagi
      panel.add(wagaLabel);
      panel.add(waga);
      //dodaj do panelu przycisk
      panel.add(oblicz);
      //dodaj do panelu etykiete BMI
      panel.add(BMILabel);
      //dodaj do panelu etykiete dla wyniku
      panel.add(wynikLabel);
      panel.add(dodatkowa);

      //dodaj panel do frame 
      frame.getContentPane().add (panel);
   }

   //-----------------------------------------------------------------
   //  Wyswietl frame aplikacji podstawowej
   //-----------------------------------------------------------------
   public void display()
   {
      frame.pack();
      frame.show();
   }
   
   public void sprawdzCzyGruby(double BMI)
   {
	   if(BMI<19)
		   dodatkowa.setText("Masz niedowage!");
	   if(BMI>=19 && BMI <=25)
		   dodatkowa.setText("Jestes idealny!");
	   if(BMI>25 && BMI<=30)
		   dodatkowa.setText("Masz nadwage!");
	   if(BMI>30)
		   dodatkowa.setText("Niestety jesteś otyly");
	   
   }

   //*****************************************************************
   //  Reprezentuje action listenera dla przycisku oblicz.
   //*****************************************************************
   private class BMIListener implements ActionListener
   {
  	   
      public void actionPerformed (ActionEvent event)
      {
         String wzrostText, wagaText;          
	double bmi, wzrostVal;
        int wagaVal;
	 
	 //pobierz tekst z pol tekstowych dla wagi i wzrostu
	 //Wykorzystaj Integer.parseInt do konwersji tekstu na integer
	 wzrostText = wzrost.getText();
	 wagaText = waga.getText();
	 try{
	 		 wzrostVal = Double.parseDouble(wzrostText);
	 		wagaVal = Integer.parseInt(wagaText);
                        bmi = wagaVal/(wzrostVal*wzrostVal);
                        wynikLabel.setText(Double.toString(bmi));
	 sprawdzCzyGruby(bmi);
	 }catch(NumberFormatException nfe){
		 JFrame error = new JFrame("J");
		 JOptionPane.showMessageDialog(error, "Niewlasciwy format danych");
	 }
      }
   }
}

